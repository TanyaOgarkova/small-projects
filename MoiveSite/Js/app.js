const API_KEY = "3a097560-6cc3-4798-868f-3f8464738a75"
const API_URL_POPULAR = "https://kinopoiskapiunofficial.tech/api/v2.2/films/top?type=TOP_100_POPULAR_FILMS"
const API_URL_SEARCH ="https://kinopoiskapiunofficial.tech/api/v2.1/films/search-by-keyword?keyword="
const API_URL_MOVIE_DETAILS = "https://kinopoiskapiunofficial.tech/api/v2.2/films/"

getMovies(API_URL_POPULAR)

async function getMovies(url) {
    const resp = await fetch(url, {
        headers: {
            "Content-Type":"application/json",
            "X-API-KEY": API_KEY,
        }
    })
    const respData = await resp.json()
    showMovies(respData)
    // const num_pages = respData.pagesCount
    // draw_pagination(num_pages)
}

function getClassByRate(vote) {
    if (vote >= 7) {
        return "green"
    } else if (vote>5) {
        return "orange"
    } else {
        return "red"
    }
}
function getNumber(num) {
    if (num.includes("%")) {
        let res =  num[0] + "." + num[1]
        return res
    }
    return num
}

function showMovies(data) {
    const moviesEl = document.querySelector(".movies")

    // Очищаем предыдущие фильмы
    document.querySelector(".movies").innerHTML = ""

    data.films.forEach((movie) => {
        const movieEl = document.createElement("div")
        movieEl.classList.add("movie")
        movieEl.innerHTML = `
        <div class="movie_cover-inner">
                <img src="${movie.posterUrlPreview}"
                    class="movier-cover" alt="${movie.nameRu}" />
                <div class="movie-cover-darkened"></div>
            </div><div class="movie_info">
                    <div class="movie_title">${movie.nameRu}</div>
                    <div class="movie_category">${movie.genres.map(
                        (genre) => ` ${genre.genre}`
                    )}</div>
                    ${movie.rating && movie.rating != 'null' && (
                    `<div class="movie_average movie_average--${getClassByRate(getNumber(movie.rating))}">${getNumber(movie.rating)}</div>`
                    ) || ''
                }
                </div> `
        movieEl.addEventListener("click", () => openModal(movie.filmId))
        moviesEl.appendChild(movieEl)
    })
}          
 // Поиск
const form = document.querySelector("form")
const search = document.querySelector(".header-search")

form.addEventListener("submit", (e) => {
    e.preventDefault();
    const apiSearchUrl = `${API_URL_SEARCH} ${search.value}`
    if (search.value) {
        getMovies(apiSearchUrl)

        search.value = ""
    }
})
 // Сброс поиска
 const button = document.querySelector(".backButton")

 button.addEventListener("click",() => {
    getMovies(API_URL_POPULAR)
 })

// Modal
const modalEl = document.querySelector(".modal")

async function openModal(id) {
    const resp = await fetch(API_URL_MOVIE_DETAILS + id, {
        headers: {
            "Content-Type":"application/json",
            "X-API-KEY": API_KEY,
        }
    })
    const respData = await resp.json()

    modalEl.classList.add("modal--show")
    document.body.classList.add(".stop-scrolling")

    modalEl.innerHTML = `  
        <div class="modal__card">
        <img class="modal__movie-backdrop" src="${respData.posterUrl}" alt="">
        <h2>
        <span class="modal__movie-title">${respData.nameRu},</span>
        <span class="modal__movie-release-year">${respData.year}</span>
        </h2>
        <ul class="modal__movie-info">
        <div class="loader"></div>
        <li class="modal__movie-genre">Жанр: ${respData.genres.map((el) => ` <span>${el.genre}</span>`)}.</li>
        ${respData.filmLength ? `<li class="modal__movie-runtime">Время: ${respData.filmLength} минут.</li>` : ''}                                                                                                       
        <li >Сайт: <a class="modal__movie-site" href="${respData.webUrl}">${respData.webUrl}</a></li>
        <li class="modal__movie-overview">Описание: ${respData.description}</li>
        </ul>
        <button type="button" class="modal__button-close">Закрыть</button>
        </div>`
    const btnClose = document.querySelector(".modal__button-close")
    btnClose.addEventListener("click", () => closeModal())
}

function closeModal() {
    modalEl.classList.remove("modal--show")
    document.body.classList.remove(".stop-scrolling")
}

window.addEventListener("click", (e) => {
    if (e.target === modalEl) {
        closeModal()
    }
})

window.addEventListener("keydown", (e) => {
    if (e.keyCode === 27) {
        closeModal()
    }
})

// К сожалению пагинацию реализовать не получилось из-за того, что кинопоиск возвращает некорректные данные

// // Пагинация
// // при нажатии на кнопку пагинации отображает страницу
// function page_click(e){
//     if (search.value){
//         let url = `${API_URL_SEARCH} ${search.value}`
//         getMovies(url + `&page=${e.srcElement.id}`)
//     }
//     else{
//         let url = API_URL_POPULAR
//         getMovies(url + `&page=${e.srcElement.id}`)
//     }
  
// }
// // рисует кнопки пагинации
// function draw_pagination(num_pages) {
//     let pagination = document.querySelector(".pagination")
//     pagination.replaceChildren()
    
//     for (i=1;i<=num_pages;i++){
//         const page = document.createElement("button")
//         page.id = i
//         page.classList.add("page")
//         page.innerHTML = i
//         page.addEventListener("click", page_click)

//         pagination.appendChild(page)
//     }
// }
