const items = document.querySelectorAll('.countdown-item > h4')
const countdownElement = document.querySelector('.countdown')
//Назначаем дату отсчета
let countdownDate = new Date(2024, 11, 18, 10, 0, 0).getTime()

function getCountdownTime() {
    // Получаем текущее время
    const now = new Date().getTime()
    // Находим разницу времени
    const differ = countdownDate - now
    // 1с = 1000 мс, 1 м = 60с, 1ч = 60м, 1д = 24ч
    // Создаем переменные в милисекундах
    const oneDay = 24 * 60 * 60 * 1000
    const oneHour = 60 * 60 * 1000
    const oneMinute = 60 * 1000

    let days = Math.floor(differ / oneDay)
    let hours = Math.floor((differ % oneDay) / oneHour)
    let minutes = Math.floor((differ % oneHour) / oneMinute)
    let seconds = Math.floor((differ % oneMinute) / 1000)

    const values = [days, hours, minutes, seconds]
    // Добавляем значение переменных на страницу
    items.forEach(function (item, index) {
        item.textContent = values[index]
    })

    if (differ < 0) {
        clearInterval(countdown)
        countdownElement.innerHTML = "<h4 class='expired'>Время истекло</h4>"
    }

}
let countdown = setInterval(getCountdownTime, 1000)

