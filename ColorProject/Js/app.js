const buttons = document.getElementsByClassName("block")



const hex = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F',]
for (var id = 0; id < buttons.length; id++) {
    let button = buttons[id]
    button.addEventListener("click", () => {
    let hexColor = generateHex()
    button.style.backgroundColor = hexColor
}
)
}

function generateHex() {
let hexColor = "#"
for (let i = 0; i<6; i++) {
     hexColor += hex[getRandomNumber()]
}
return hexColor
}


function getRandomNumber() {
    return Math.floor(Math.random() * hex.length)
}