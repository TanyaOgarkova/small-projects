const API_URL_CHICKEN ='https://api.edamam.com/api/recipes/v2?type=public&app_id=feaf6a5d&app_key=9843b706ca39c735bcc91928dd2d4285&field=ingredients&field=images&field=label&q="chicken"'
const API_URL_SEARCH ='https://api.edamam.com/api/recipes/v2?type=public&app_id=feaf6a5d&app_key=9843b706ca39c735bcc91928dd2d4285&field=ingredients&field=images&field=label&q='
let next_page = '';

async function getRecipes(url) {
    const resp = await fetch(url, {
        headers: {
            "Content-Type":"application/json",
        }
    })
    const respData = await resp.json()
    showRecipes(respData)
    next_page = respData._links.next.href
    document.getElementById('next-page').style ='visibility: visible';
}
function showRecipes(data) {
    const RecipesEl = document.querySelector(".recipes")

    data.hits.forEach((recipe) => {
        const recipeEl = document.createElement("div")
        recipeEl.classList.add("recipe")
        recipeEl.innerHTML = `
                <div class="recipe_inner">
                     <img class ="img" src="${recipe.recipe.images.REGULAR.url}">
                     </div>
                <div class="recipe_info">
                <div class="recipe_title">${recipe.recipe.label}</div>
                <div class="recipe_ingr">${recipe.recipe.ingredients.map(
                        (ing) => ` ${ing.text}` 
                    )} </div>
                </div>
                `
        recipeEl.addEventListener("click", () => openModal(recipe._links.self.href+"&field=sorce&field=label&field=url&field=images"))
        RecipesEl.appendChild(recipeEl)
    })
}     

getRecipes(API_URL_CHICKEN)
 // Поиск
const form = document.querySelector("form")
const search = document.querySelector(".header-search")

form.addEventListener("submit", (e) => {
    e.preventDefault();

    document.getElementById('next-page').style ='visibility: hidden';

    // Очищаем предыдущие рецепты
    document.querySelector(".recipes").innerHTML = ""

    const apiSearchUrl = `${API_URL_SEARCH} ${search.value}`
    if (search.value) {
        getRecipes(apiSearchUrl)
    }
    search.value = ""
})
 // Сброс поиска
 const button = document.querySelector(".backButton")
 button.addEventListener("click",() => {
    
    // Очищаем предыдущие рецепты
    document.querySelector(".recipes").innerHTML = ""
    getRecipes(API_URL_CHICKEN)
    search.value = ""
    document.getElementById('next-page').style ='visibility: hidden';
 })

// // Modal
const modalEl = document.querySelector(".modal")

async function openModal(recipe_url) {
    const resp = await fetch(recipe_url, {
        headers: {
            "Content-Type":"application/json",
        }
    })
    const respData = await resp.json()

    modalEl.classList.add("modal--show")
    document.body.classList.add(".stop-scrolling")

    modalEl.innerHTML = `  
        <div class="modal__card">
        <img class="modal__recipe-backdrop" src="${respData.recipe.images.REGULAR.url}" alt="">
        <h2>
        <div class="modal__recipe-title">${respData.recipe.label}</div>
        </h2>
        <div class="modal__recipe-overview">Recipe: <a href="${respData.recipe.url}">${respData.recipe.url}</a></div>        
        <button type="button" class="modal__button-close">Close</button>
        </div>`
    const btnClose = document.querySelector(".modal__button-close")
    btnClose.addEventListener("click", () => closeModal())
}

function closeModal() {
    modalEl.classList.remove("modal--show")
    document.body.classList.remove(".stop-scrolling")
}

window.addEventListener("click", (e) => {
    if (e.target === modalEl) {
        closeModal()
    }
})

window.addEventListener("keydown", (e) => {
    if (e.keyCode === 27) {
        closeModal()
    }
}) 
 
 // Кнопка - загрузить ещё
 const loading = document.querySelector(".loading-button")
 loading.addEventListener("click",() => {
    getRecipes(next_page)
 })


  
  


